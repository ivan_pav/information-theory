﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        const int n = 256;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Text = "Лаб. раб. №1 Чижик, Павлович";
            textBox1.Text = "Введите исходный текст";
            textBox3.Text = "3";
            textBox4.Text = "Введите шифрованный текст";
            textBox6.Text = "5";
            textBox7.Text = Convert.ToString(n);
            textBox8.Text = Convert.ToString(n);
        }

        private void codeButton_Click(object sender, EventArgs e)
        {
            string data = textBox1.Text;
            int key = int.Parse(textBox3.Text);
            string result = "";
            foreach (char character in data)
            {
                result += (char)(character * key % n);
            }
            textBox2.Text = result;
        }

        private void decodeButton_Click(object sender, EventArgs e)
        {
            string data = textBox4.Text;
            int key = int.Parse(textBox6.Text);
            string result = "";
            foreach (char character in data)
            {
                for (int i = 0; i < n; i++)
                {
                    if ((i * key - (int)character) % n == 0)
                    {
                        result += (char)(i);
                    }
                }
            }
            textBox5.Text = result;
        }
    }
}
